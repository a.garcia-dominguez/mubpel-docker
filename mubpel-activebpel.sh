#!/bin/bash

docker run -i -v $(pwd):/work -u 0:0 \
       registry.gitlab.com/a.garcia-dominguez/mubpel-docker/activebpel:latest "$@"
