#!/bin/bash

export ACTIVEBPEL_DIR=/work/activebpel

start_activebpel() {
	output=$($EXEC mktemp "${TMPDIR:-/tmp/}activepbel.XXX")
	$EXEC java -jar /app/activebpel-jetty.jar "$ACTIVEBPEL_DIR" "${ACTIVEBPEL_PORT:-8080}" none 2>&1 | $EXEC tee $output &>/dev/null &
	ACTIVEBPEL_PID=$!
	until grep -q -i "ActiveBPEL is now RUNNING" $output; do
		if ! ps $ACTIVEBPEL_PID > /dev/null; then
			echo "ActiveBPEL did not start" >&2
			exit 1
		fi
		sleep 1
	done
}

if [ "$(id -u)" = "0" ]; then
    # From 'Tips and Tricks of the Docker Captains' (Docker channel, YouTube)
    # https://youtu.be/woBI466WMR8
    #
    # running on a dev machine: fix perms then run as unprivileged user
    fix-perms -r -u mubpel -g mubpel /work 2>/dev/null >/dev/null

    export EXEC="exec runuser -u mubpel --"
else
    export EXEC=exec
fi

MUBPEL_SCRIPT="$1"
MUBPEL_COMMAND="$2"
shift 2

if egrep -q -w 'compare|comparefull|run' <<<"$MUBPEL_COMMAND"; then
	# Using one of the commands that requires a running engine
    if ! start_activebpel; then
    	exit 1
    fi
    $EXEC "$MUBPEL_SCRIPT" "$MUBPEL_COMMAND" --engine-type activebpel --bpr-directory "$ACTIVEBPEL_DIR/bpr" "$@"
else
	$EXEC "$MUBPEL_SCRIPT" "$MUBPEL_COMMAND" "$@"
fi
