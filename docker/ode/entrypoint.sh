#!/bin/sh

# From 'Tips and Tricks of the Docker Captains' (Docker channel, YouTube)
# https://youtu.be/woBI466WMR8

if [ "$(id -u)" = "0" ]; then
    # running on a dev machine: fix perms then run as unprivileged user
    fix-perms -r -u mubpel -g mubpel /work 2>/dev/null >/dev/null
    exec runuser -u mubpel -- "$@"
else
    # run as usual
    exec "$@"
fi
