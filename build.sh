#!/bin/bash

docker build -t mubpel-ode:latest docker/ode "$@"
docker build -t mubpel-activebpel:latest docker/activebpel "$@"
