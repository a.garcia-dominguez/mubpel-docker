# Docker images for MuBPEL

This is a set of Docker images for the [MuBPEL](https://gitlab.com/ucase/public/ws-bpel-testing-tools/-/wikis/MuBPEL) WS-BPEL mutation testing tool.
The images are automatically rebuilt each week with the latest version of MuBPEL and OpenJDK 8.

## Run MuBPEL with ODE

Download the [mubpel-ode.sh](mubpel-ode.sh) script and put it in a folder in your `PATH`.
Make sure it is executable, and then run it from the folder with your BPEL files.
For instance, to analyze `/your/directory/process.bpel`:

```sh
cd /your/directory
mubpel-ode.sh analyze process.bpel
```

The Docker image runs with an unprivileged user, whose UID/GID are automatically changed to match the UID and GID of the owner of the current folder.

## Run MuBPEL with ActiveBPEL

Same as above, but use [mubpel-activebpel.sh](mubpel-activebpel.sh) instead.

## Update MuBPEL

To update the MuBPEL images in your computer, run the provided [mubpel-update.sh](mubpel-update.sh) script in this repository:

```sh
./mubpel-update.sh
```

## Build locally

To test the build locally, run this script:

```sh
./build.sh
```

## Run a local image

To run a locally-built image, use the `mubpel-ode-local.sh` or `mubpel-activebpel-local.sh` scripts, passing any arguments as usual.
